#pragma once
#include "Map.h"


void Map::GenerateBorders() {

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            if (i == 0 || i == N - 1) {
                grid[i][j] = border;
            }
            else if (j == 0 || j == M - 1) {
                grid[i][j] = border;
            }
            else grid[i][j] = prefab;
        }
    }

}




bool Map::around(int X, int Y, int W, int H)
{
    bool check;
    for (int i = Y; i < H + Y; i++)
    {
        if (grid[X - 1][i] == prefab)
            check = true;
        else return false;
    }
    for (int i = Y; i < H + Y; i++)
    {
        if (grid[X + W + 1][i] == prefab)
            check = true;
        else return false;
    }
    for (int i = X; i < W + X; i++)
    {
        if (grid[i][Y - 1] == prefab)
            check = true;
        else return false;
    }
    for (int i = X; i < W + X; i++)
    {
        if (grid[i][Y + H + 1] == prefab)
            check = true;
        else return false;
    }
    return true;
}



void Map::TrapGenerate()
{
    int trapX = 0;
    int trapY = 0;
    while (numberOfTraps > 0) {
        trapX = rand() % N;
        trapY = rand() % M;
        int trapY = rand() % M;
        if (around(trapX, trapY, 5, 5)) {
            grid[trapX][trapY] = trap;
            numberOfTraps--;
        }
    }
}


Map::Map() {
	map_image.loadFromFile("hero textures/map2.jpg");
	map.loadFromImage(map_image);
    traps.loadFromFile("hero textures/Spike Trap.png");
    s_trap.setTexture(traps);
	s_map.setTexture(map);
    numberOfTraps = (M * N) / 100;


    GenerateBorders();
    TrapGenerate();

}




sf::Sprite Map::drawMap(int i, int j) {
    if (grid[i][j] == ' ' || grid[i][j] == '0'){
        if (grid[i][j] == ' ')  s_map.setTextureRect(sf::IntRect(65, 0, 32, 32));
        if ((grid[i][j] == '0')) s_map.setTextureRect(sf::IntRect(33, 0, 32, 32));
        s_map.setPosition(j * 32, i * 32);//�� ���� ����������� ����������, ��������� � �����.
        return s_map;
    }
    else {
        if ((grid[i][j] == '@')) {
            if (needToDrawTrap) {
                s_trap.setPosition(j * 32, i * 32);
                needToDrawTrap = false;
                return s_trap;
            }
            else {
                s_map.setTextureRect(sf::IntRect(65, 0, 32, 32));
                needToDrawTrap = true;
                s_map.setPosition(j * 32, i * 32);
                return s_map;
            }
        }
    }




}

void Map::update(float elapsedTime) {
    vrem += elapsedTime;
    if (kadr >= 15) { kadr = 1; }
    if (kadr == 1) {
        s_trap.setTextureRect(sf::IntRect(0, 0, 32, 32));
        kadr++;
    }
    else if (vrem > 0.1) {
        s_trap.setTextureRect(sf::IntRect(32 * (kadr-1), 0, 32,32));
        vrem = 0;
        kadr++;
    }
}

 
Map* Map::returnMap() {
	return this;
}


