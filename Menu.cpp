#include "Menu.h"


Menu::Menu(float width, float height)
{
	if (!font.loadFromFile("hero textures/arial.ttf")) {
	}

	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::Red);
	menu[0].setString("Continue");
	menu[0].setCharacterSize(50);
	menu[0].setPosition(sf::Vector2f(width- 65, height - 150));

	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::White);
	menu[1].setString("Power up");
	menu[1].setCharacterSize(50);
	menu[1].setPosition(sf::Vector2f(width - 65, height-50));

	menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::White);
	menu[2].setString("Exit");
	menu[2].setCharacterSize(50);
	menu[2].setPosition(sf::Vector2f(width - 65, height+50));

	selectedItemIndex = 0;
}


Menu::~Menu()
{
}

void Menu::draw(sf::RenderWindow* window)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		window->draw(menu[i]);
	}
}

void Menu::MoveUp()
{
	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex--;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void Menu::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex++;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

PowerUp::PowerUp(float width, float height, Hero* hero)
{
	if (!font.loadFromFile("hero textures/undertale battle font_0.ttf")) {
	}
	this->speed = hero->hero_speed;
	this->health = hero->maxHealth;
	this->damage = hero->damage;
	this->hero = hero;


	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::Red);
	if (speed == 200) {
		menu[0].setString("Speed lvlMax");
	}
	else {
		menu[0].setString(to_string((speed+50) * 10) + "$  Speed + 50");
	}
	menu[0].setCharacterSize(50);
	menu[0].setPosition(sf::Vector2f(width - 200, height - 150));
	
	

	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::White);
	if (health == 300) {
		menu[1].setString(" Health lvlMax");
	}
	else {
		menu[1].setString(to_string((health+50) * 5) + "$ Health + 50");
	}
	
	menu[1].setCharacterSize(50);
	menu[1].setPosition(sf::Vector2f(width - 200, height - 50));


	menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::White);
	if (damage == 100) {
		menu[2].setString(" Damage lvlMax");
	}
	else {
		menu[2].setString(to_string((damage+20) * 50) + "$ Damage + 20");
	}

	menu[2].setCharacterSize(50);
	menu[2].setPosition(sf::Vector2f(width - 200, height + 50));

	

	menu[3].setFont(font);
	menu[3].setFillColor(sf::Color::White);
	menu[3].setString("back");
	menu[3].setCharacterSize(50);
	menu[3].setPosition(sf::Vector2f(width - 150, height + 150));

	selectedItemIndex = 0;
}


PowerUp::~PowerUp()
{
}

void PowerUp::draw(sf::RenderWindow* window)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS2; i++)
	{
		window->draw(menu[i]);
	}
}

void PowerUp::MoveUp()
{
	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex--;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void PowerUp::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS2)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex++;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void PowerUp::speedUp() {
	if (speed < 200) {
		if (hero->money >= (speed + 50) * 10) {
			speed+=50;
			hero->hero_speed += 50;
			hero->payMoney(-(speed * 10));
		}
	}
}

void PowerUp::healthUp() {
	if (health < 300) {
		if (hero->money >= (health+50) * 5) {
			health+=50;
			hero->maxHealth += 50;
			hero->payMoney(-(health * 5));
		}
	}
}

void PowerUp::damageUp() {
	if (damage < 100) {
		if (hero->money >= (damage+20) * 50) {
			damage+=20;
			hero->damage += 20;
			hero->payMoney(-(damage * 50));	
		}
	}
}