#include "Engine.h"

void Engine::draw()
{
    // ������� ���������� ����
    m_Window->clear(Color::Black);
	m_Window->setView(hero->getCamera());

    // ������������ �����
    for (int i = hero->getHeroVision1().y; i <= hero->getHeroVision2().y; i++)
        for (int j = hero->getHeroVision1().x; j <= hero->getHeroVision2().x; j++)
        {
            m_Window->draw(map->drawMap(i, j));//������ ���������� �� �����
            if (map->needToDrawTrap) {
                m_Window->draw(map->drawMap(i, j));
            }
        }

    m_Window->draw(hero->getSprite());

    for (int i = Enemy::n; i > 0; i--) {
        m_Window->draw(Enemy::thisEnemy->enemy->getSprite());
        if (Enemy::n <= 1) { continue; }
        Enemy::thisEnemy = Enemy::thisEnemy->next;
    }
    
    for (int i = Coin::coins; i > 0; i--) {
        m_Window->draw(Coin::thisCoin->coin->getSprite());
        if (Coin::coins <= 1) { continue; }
        Coin::thisCoin = Coin::thisCoin->next;
    }

    m_Window->draw(hero->getCircle());
    m_Window->draw(hero->moneyBa);
    m_Window->draw(hero->moneyText);
    m_Window->draw(hero->timeText);
    m_Window->draw(hero->healthBa);
    // ���������� ���, ��� ����������

    m_Window->display();
}


