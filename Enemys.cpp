#include "Enemys.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>


Enemy::EnemyList* Enemy::lastEnemy = NULL;
Enemy::EnemyList* Enemy::thisEnemy = NULL;
int Enemy::n = 0;

Enemy::Enemy(Map* map, Hero* hero, bool isBoss)
{
    //
    texture_size = 32;
    kadr = 1;
    vrem = 0;
    this->isBoss = isBoss;
    //������ �� ������ ��������
    this->map = map;
    this->hero = hero;
    //
    bufferTakeHit.loadFromFile("hero textures/enemy_takeHit.ogg");
    takeHit.setBuffer(bufferTakeHit);
    takeHit.setVolume(5);
    int place = rand() % 4;
    if (place == 0) {
        enemy_Position.x = hero->getHeroVision1().x * texture_size;
        enemy_Position.y = hero->getHeroVision1().y * texture_size;
    }
    if (place == 1) {
        enemy_Position.x = hero->getHeroVision2().x * texture_size;
        enemy_Position.y = hero->getHeroVision1().y * texture_size;
    }
    if (place == 2) {
        enemy_Position.x = hero->getHeroVision1().x * texture_size;
        enemy_Position.y = hero->getHeroVision2().y * texture_size;
    }
    if (place == 3) {
        enemy_Position.x = hero->getHeroVision2().x * texture_size;
        enemy_Position.y = hero->getHeroVision2().y * texture_size;
    }
    
}

// ������ ��������� ������ ��������� ��� ������� draw()
sf::Sprite Enemy::getSprite()
{
    return enemy_Sprite;
}


sf::Vector2f Enemy::getPosition() {
    return enemy_Position;
}

void Enemy::checkHero(sf::Vector2f hero_Position) {
    if (enemy_Position.x - hero_Position.x  > 30 * 0.9) {
        moveLeft();
        
    }
    else if (hero_Position.x - enemy_Position.x > 31 * 0.9) {
        moveRight();
    }
    else {
        stopRight();
        stopLeft();
    }
    //
    if (enemy_Position.y - hero_Position.y > 30 * 0.9) {
        moveUp();
    }
    else if (hero_Position.y - enemy_Position.y > 31 * 0.9) {
        moveDown();
    }
    else {
        stopDown();
        stopUp();
    }
}


bool Enemy::checkAtack() {
    if (abs(hero->getPosition().x - enemy_Position.x) < 300 &&
        abs(hero->getPosition().y - enemy_Position.y) < 300) {
        if (hero->hero_lighter2) {
            enemy_attackedFromTrap = true;
            if (timeAttacked > 0.3) {
                timeAttacked = 0;
                health -= 1* (hero->damage / 20);
            }
        }
    }

    if (hero->hero_atack2) {
        if (hero->kadrAttack == 5 || hero->kadrAttack == 6 ||
            hero->kadrAttack == 7) {
            if (heroAttackKadr != hero->kadrAttack) {
                if (abs(hero->getPosition().x - enemy_Position.x) < 40 &&
                    abs(hero->getPosition().y - enemy_Position.y) < 40) {

                    enemy_Sprite.setColor(sf::Color::Red);
                    health -= hero->returnDamage();
                    takeHit.play();

                }
            }
        }
        else {
            enemy_Sprite.setColor(sf::Color::White);
        }
    }
    else {
        enemy_Sprite.setColor(sf::Color::White);
    }

    if (health <= 0) {
        enemy_speed = 0;
        dying = true;
        //hero->payMoney(20);
        return true;
    }
    heroAttackKadr = hero->kadrAttack;
    return false;
}

void Enemy::moveLeft()
{
    enemy_LeftPressed = true;
    enemy_Sprite.setScale(-enemy_scale, enemy_scale);
}

void Enemy::moveRight()
{
    enemy_RightPressed = true;
    enemy_Sprite.setScale(enemy_scale, enemy_scale);
}

void Enemy::moveUp()
{
    enemy_UpPressed = true;
}

void Enemy::moveDown()
{
    enemy_DownPressed = true;
}

void Enemy::stopLeft()
{
    enemy_LeftPressed = false;
}

void Enemy::stopRight()
{
    enemy_RightPressed = false;
}

void Enemy::stopUp()
{
    enemy_UpPressed = false;
}

void Enemy::stopDown()
{
    enemy_DownPressed = false;
}


// �������  �� ��������� ����������������� ����� � ���� �����,
// ���������� ������� � ��������
void Enemy::update(float elapsedTime)
{
    vrem += elapsedTime;

    if (dying) { 
        dieAnim(vrem);
        return ;
    }

    checkHero(hero->getPosition());

    if ((kadrAttack == 6 || kadrAttack == 7 || kadrAttack == 8)) {
        hero_attacked = true;
    }
    else {
        hero_attacked = false;
    }

    if (checkAtack()) {
        return;
    }
    
    trapAttacked(elapsedTime);
    if (enemy_RightPressed)
    {
        enemy_Position.x += enemy_speed * elapsedTime;
        moveRightAnim();
    }

    if (enemy_LeftPressed)
    {
        enemy_Position.x -= enemy_speed * elapsedTime;
        moveRightAnim();
    }

    if (enemy_UpPressed)
    {
        enemy_Position.y -= enemy_speed * elapsedTime;
        moveRightAnim();
    }

    if (enemy_DownPressed)
    {
        enemy_Position.y += enemy_speed * elapsedTime;
        moveRightAnim();
    }

    if (!isBoss){ enemyLimitation(); }
    
    if (!enemy_RightPressed && !enemy_LeftPressed && !enemy_UpPressed && !enemy_DownPressed) {
        atackAnim();
    }
    else {
        kadrAttack = 1;
    }


    
    // ������ �������� ������ �� ����� �������
    enemy_Sprite.setPosition(enemy_Position);
}



void Enemy::enemyLimitation(/*char direction*/) {

    EnemyList* thisEnemy = Enemy::thisEnemy;
    if (enemy_UpPressed/*direction == 'w'*/) {
        for (int i = enemy_Position.x - enemy_width / 2; i < (enemy_Position.x + enemy_width / 2); i += enemy_width / 4) {
            if (map->grid[int((enemy_Position.y - enemy_height / 2) / texture_size)][int(i / texture_size)] == '0') {
                enemy_Position.y = int(enemy_Position.y / texture_size) * texture_size + enemy_height / 2;
                break;
            }
        }


            for (int j = n; j > 1; j--) {
                if (enemy_Position.y < thisEnemy->next->enemy->getPosition().y) { 
                    thisEnemy = thisEnemy->next;
                    continue;  }
                if (enemy_Position.y - thisEnemy->next->enemy->getPosition().y < enemy_height / 4
                    && abs(thisEnemy->next->enemy->getPosition().x - enemy_Position.x) < enemy_width / 4) {
                    //enemy_Position.y = thisEnemy->next->enemy->getPosition().y + enemy_height/4 + 1;
                    enemy_Position.y += (enemy_height / 4) - abs(thisEnemy->next->enemy->getPosition().y - enemy_Position.y) + 1;
                    //enemy_speed = -1;  
                    //break;
                }
                thisEnemy = thisEnemy->next;
            }
            thisEnemy = Enemy::thisEnemy;
    }
    
        if (enemy_DownPressed/*direction == 's'*/) {
            for (int i = enemy_Position.x - enemy_width / 2; i < (enemy_Position.x + enemy_width / 2); i += enemy_width / 4) {
                if (map->grid[int((enemy_Position.y + enemy_height / 2) / texture_size)][int(i / texture_size)] == '0') {
                    enemy_Position.y = int(enemy_Position.y / texture_size) * texture_size + texture_size - enemy_height / 2;
                    break;
                }
            }


                for (int j = n; j > 1; j--) {
                    if (enemy_Position.y > thisEnemy->next->enemy->getPosition().y) {
                        thisEnemy = thisEnemy->next;
                        continue;
                    }
                    if (enemy_Position.y - thisEnemy->next->enemy->getPosition().y > -(enemy_height / 4)
                        && abs(thisEnemy->next->enemy->getPosition().x - enemy_Position.x) < enemy_width / 4) {
                        //enemy_Position.y = thisEnemy->next->enemy->getPosition().y - enemy_height / 4 - 1;
                        enemy_Position.y -= (enemy_height / 4) - abs(thisEnemy->next->enemy->getPosition().y - enemy_Position.y) - 1;
                        
                        //enemy_speed = 0;
                        //break;
                    }       
                    thisEnemy = thisEnemy->next;
                }
                thisEnemy = Enemy::thisEnemy;


            
        }
        if (enemy_LeftPressed/*direction == 'l'*/) {
            for (int i = enemy_Position.y - enemy_height / 2; i < (enemy_Position.y + enemy_height / 2); i += enemy_height / 4) {
                if (map->grid[int(i / texture_size)][int((enemy_Position.x - enemy_width / 2) / texture_size)] == '0') {
                    enemy_Position.x = int((enemy_Position.x) / texture_size) * texture_size + enemy_width / 2;
                    break;
                }
            }


            
                for (int j = n; j > 1; j--) {
                    if (enemy_Position.x < thisEnemy->next->enemy->getPosition().x) { 
                        thisEnemy = thisEnemy->next;
                        continue; }
                    if (enemy_Position.x - thisEnemy->next->enemy->getPosition().x  < enemy_width / 4
                        && abs(thisEnemy->next->enemy->getPosition().y - enemy_Position.y) < enemy_height / 4) {
                        //enemy_Position.x = thisEnemy->next->enemy->getPosition().x + enemy_width / 4 + 1;
                        enemy_Position.x += (enemy_width / 4) - abs(thisEnemy->next->enemy->getPosition().x - enemy_Position.x) + 1;
                        //enemy_speed = -10;
                        //break;
                    }
                    thisEnemy = thisEnemy->next;
                }
                thisEnemy = Enemy::thisEnemy;


            
        }
        if (enemy_RightPressed/*direction == 'r'*/) {
            for (int i = enemy_Position.y - enemy_height / 2; i < (enemy_Position.y + enemy_height / 2); i += enemy_height / 4) {
                if (map->grid[int(i / texture_size)][int((enemy_Position.x + enemy_width / 2) / texture_size)] == '0') {
                    enemy_Position.x = int(enemy_Position.x / texture_size) * texture_size + texture_size - enemy_width / 2;
                    break;
                }
            }



           
                for (int j = n; j > 1; j--) {
                    if (enemy_Position.x > thisEnemy->next->enemy->getPosition().x) {
                        thisEnemy = thisEnemy->next;
                        continue;
                    }
                    if (enemy_Position.x - thisEnemy->next->enemy->getPosition().x > -(enemy_width / 4)
                        && abs(thisEnemy->next->enemy->getPosition().y - enemy_Position.y) < enemy_height / 4) {
                        //enemy_Position.x = thisEnemy->next->enemy->getPosition().x - enemy_width / 4 - 1;
                        enemy_Position.x -= (enemy_width / 4) - abs(thisEnemy->next->enemy->getPosition().x - enemy_Position.x) - 1;
                        //enemy_speed = 0;
                        //break;
                    }
                    thisEnemy = thisEnemy->next;
                }
                thisEnemy = Enemy::thisEnemy;
            }
     
}


void Enemy::trapAttacked(float vrem) {
    if (enemy_attackedFromTrap) {
        enemy_Sprite.setColor(sf::Color::Red);
        timeAttacked += vrem;
        if (timeAttacked > 1.4) {
            enemy_attackedFromTrap = false;
        }
        else if (timeAttacked > 0.3) {
            enemy_Sprite.setColor(sf::Color::White);
        }
    }
    else {
        
        if (map->grid[int(enemy_Position.y / texture_size)][int(enemy_Position.x / texture_size)] == '@')
        {
            if (!enemy_attackedFromTrap) {
                if (map->kadr >= 9 && map->kadr <= 12) {
                    enemy_attackedFromTrap = true;
                    health-=20;
                    timeAttacked = 0;
                }
            }

        }
    }
}


Goblin::Goblin(Map* map, Hero* hero, bool isBoss): Enemy(map, hero, isBoss) {
    // ��������� �������� �����
    if (isBoss) {
        enemy_scale = 1.6;
        health = 200 * enemy_scale * 3;
        enemy_speed = 70;
        enemy_height = 40 * enemy_scale;
        enemy_width = 36 * enemy_scale;
        cost = 20 * enemy_scale * 3;
        damage = 10 * 3;
    }
    else {
        enemy_scale = 0.8;
        health = 200;
        enemy_speed = 70;
        enemy_height = 40 * enemy_scale;
        enemy_width = 36 * enemy_scale;
        cost = 20;
        damage = 10;
    }
    

    // ��������
    enemy_Texture.loadFromFile("hero textures/Monsters/Goblin/idle.png");
    enemy_Run_Texture.loadFromFile("hero textures/Monsters/Goblin/Run.png");
    enemy_Attack_Texture.loadFromFile("hero textures/Monsters/Goblin/Attack.png");
    enemy_dying_Texture.loadFromFile("hero textures/Monsters/Goblin/Death.png");
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(55, 62, 36, 40));
    //


    // ������������� ��������� ������� ����� � ��������
    enemy_Sprite.setScale(enemy_scale, enemy_scale);
    enemy_Sprite.setOrigin(enemy_width / 2, enemy_height / 2);//������������� �����
  
}


void Goblin::moveRightAnim() {
    if (kadr == 9) { kadr = 1; }
    if (kadr == 1) {
        enemy_Sprite.setTexture(enemy_Run_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(55, 62, 36, 40));
        kadr++;
    }
    else if (vrem > 0.07) {
        enemy_Sprite.setTextureRect(sf::IntRect(55 + (kadr - 1) * 114 + 36 * (kadr - 1), 62, 36, 40));
        vrem = 0;
        kadr++;
    }
}

void Goblin::stayingAnim() {
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(55, 62, 36, 40));
    kadr = 1;
    vrem = 0;
}

void Goblin::atackAnim() {   

    if (kadrAttack >= 9) { kadrAttack = 1; }
    if (kadrAttack == 1) {
        enemy_Sprite.setTexture(enemy_Attack_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(55, 62, 36, 40));
        kadrAttack++;
    }
    else if (vrem > 0.06) {
        enemy_Sprite.setTextureRect(sf::IntRect(55 + (kadrAttack - 1) * 114 + 36 * (kadrAttack - 1), 62, 36, 40));
        vrem = 0;
        kadrAttack++;
    }
    if (kadrAttack == 8) {
        if (flagOfAttack == true){
            hero->getDamage(damage);
            flagOfAttack = false;
        }
    }
    else {
        flagOfAttack = true;
    }

    
}

void Goblin::dieAnim(double vrem) {
    enemy_Sprite.setTexture(enemy_dying_Texture);
    if (dieKadr == 1) { enemy_Sprite.setTextureRect(sf::IntRect(56, 63, 44, 22)); }
    
    if (dieKadr == 2) { enemy_Sprite.setTextureRect(sf::IntRect(209, 61, 36, 39)); }
    
    if (dieKadr == 3) { enemy_Sprite.setTextureRect(sf::IntRect(357, 77, 38, 25)); }
    
    if (dieKadr == 4) { enemy_Sprite.setTextureRect(sf::IntRect(507, 85, 52, 16)); }
    
    if(dieKadr == 5){ deleteEnemyList(this); }
    if (vrem > 0.2) {
        dieKadr++;
        this->vrem = 0;
    }
}

Skeleton::Skeleton(Map* map, Hero* hero, bool isBoss) : Enemy(map, hero, isBoss) {
    // ��������� �������� �����
    if (isBoss) {
        enemy_scale = 1.6;
        health = 160 * enemy_scale * 3;
        enemy_speed = 70;
        enemy_height = 63 * enemy_scale;
        enemy_width = 50 * enemy_scale;
        cost = 20 * enemy_scale * 3;
        damage = 10 * 3;
    }
    else {
        enemy_scale = 0.8;
        health = 160;
        enemy_speed = 70;
        enemy_height = 63 * enemy_scale;
        enemy_width = 50 * enemy_scale;
        cost = 20;
        damage = 10;
    }
    

    // ��������
    enemy_Texture.loadFromFile("hero textures/Monsters/Skeleton/Idle.png");
    enemy_Run_Texture.loadFromFile("hero textures/Monsters/Skeleton/Walk.png");
    enemy_Attack_Texture.loadFromFile("hero textures/Monsters/Skeleton/Attack.png");
    enemy_dying_Texture.loadFromFile("hero textures/Monsters/Skeleton/Death.png");
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
    //


    // ������������� ��������� ������� ����� � ��������
    enemy_Sprite.setScale(enemy_scale, enemy_scale);
    enemy_Sprite.setOrigin(enemy_width / 2, enemy_height / 2);//������������� �����

}


void Skeleton::moveRightAnim() {
    if (kadr == 5) { kadr = 1; }
    if (kadr == 1) {
        enemy_Sprite.setTexture(enemy_Run_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
        kadr++;
    }
    else if (vrem > 0.07) {
        enemy_Sprite.setTextureRect(sf::IntRect(52 + (kadr - 1) * 102 + 48 * (kadr - 1), 48, 54, 54));
        vrem = 0;
        kadr++;
    }
}

void Skeleton::stayingAnim() {
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
    kadr = 1;
    vrem = 0;
}

void Skeleton::atackAnim() {

    if (kadrAttack >= 9) { kadrAttack = 1; }
    if (kadrAttack == 1) {
        enemy_Sprite.setTexture(enemy_Attack_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
        kadrAttack++;
    }
    else if (vrem > 0.06) {
        enemy_Sprite.setTextureRect(sf::IntRect(52 + (kadrAttack - 1) * 102 + 48 * (kadrAttack - 1), 48, 58, 54));
        vrem = 0;
        kadrAttack++;
    }
    if (kadrAttack == 8) {
        if (flagOfAttack == true) {
            hero->getDamage(damage);
            flagOfAttack = false;
        }
    }
    else {
        flagOfAttack = true;
    }


}

void Skeleton::dieAnim(double vrem) {
    enemy_Sprite.setTexture(enemy_dying_Texture);
    if (dieKadr == 1) { enemy_Sprite.setTextureRect(sf::IntRect(56, 63, 44, 22)); }

    if (dieKadr == 2) { enemy_Sprite.setTextureRect(sf::IntRect(209, 61, 36, 39)); }

    if (dieKadr == 3) { enemy_Sprite.setTextureRect(sf::IntRect(357, 77, 38, 25)); }

    if (dieKadr == 4) { enemy_Sprite.setTextureRect(sf::IntRect(507, 85, 52, 16)); }

    if (dieKadr == 5) { deleteEnemyList(this); }
    if (vrem > 0.2) {
        dieKadr++;
        this->vrem = 0;
    }
}

Mushroom::Mushroom(Map* map, Hero* hero, bool isBoss) : Enemy(map, hero, isBoss) {
    // ��������� �������� �����
    if (isBoss) {
        enemy_scale = 2.0;
        health = 400 * enemy_scale * 3;
        enemy_speed = 35;
        enemy_height = 36 * enemy_scale;
        enemy_width = 27 * enemy_scale;
        cost = 30 * enemy_scale * 3;
        damage = 20 * 3;
    }
    else {
        enemy_scale = 1.0;
        health = 400;
        enemy_speed = 35;
        enemy_height = 36 * enemy_scale;
        enemy_width = 27 * enemy_scale;
        cost = 30;
        damage = 20;
    }
    

    // ��������
    enemy_Texture.loadFromFile("hero textures/Monsters/Mushroom/Idle.png");
    enemy_Run_Texture.loadFromFile("hero textures/Monsters/Mushroom/Run.png");
    enemy_Attack_Texture.loadFromFile("hero textures/Monsters/Mushroom/Attack.png");
    enemy_dying_Texture.loadFromFile("hero textures/Monsters/Mushroom/Death.png");
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
    //


    // ������������� ��������� ������� ����� � ��������
    enemy_Sprite.setScale(enemy_scale, enemy_scale);
    enemy_Sprite.setOrigin(enemy_width / 2, enemy_height / 2);//������������� �����

}


void Mushroom::moveRightAnim() {
    if (kadr == 9) { kadr = 1; }
    if (kadr == 1) {
        enemy_Sprite.setTexture(enemy_Run_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
        kadr++;
    }
    else if (vrem > 0.07) {
        enemy_Sprite.setTextureRect(sf::IntRect(52 + (kadr - 1) * 102 + 48 * (kadr - 1), 48, 54, 54));
        vrem = 0;
        kadr++;
    }
}

void Mushroom::stayingAnim() {
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
    kadr = 1;
    vrem = 0;
}

void Mushroom::atackAnim() {

    if (kadrAttack >= 9) { kadrAttack = 1; }
    if (kadrAttack == 1) {
        enemy_Sprite.setTexture(enemy_Attack_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
        kadrAttack++;
    }
    else if (vrem > 0.06) {
        enemy_Sprite.setTextureRect(sf::IntRect(52 + (kadrAttack - 1) * 102 + 48 * (kadrAttack - 1), 48, 58, 54));
        vrem = 0;
        kadrAttack++;
    }
    if (kadrAttack == 8) {
        if (flagOfAttack == true) {
            hero->getDamage(damage);
            flagOfAttack = false;
        }
    }
    else {
        flagOfAttack = true;
    }


}

void Mushroom::dieAnim(double vrem) {
    enemy_Sprite.setTexture(enemy_dying_Texture);
    if (dieKadr == 1) { enemy_Sprite.setTextureRect(sf::IntRect(56, 63, 44, 22)); }

    if (dieKadr == 2) { enemy_Sprite.setTextureRect(sf::IntRect(209, 61, 36, 39)); }

    if (dieKadr == 3) { enemy_Sprite.setTextureRect(sf::IntRect(357, 77, 38, 25)); }

    if (dieKadr == 4) { enemy_Sprite.setTextureRect(sf::IntRect(507, 85, 52, 16)); }

    if (dieKadr == 5) { deleteEnemyList(this); }
    if (vrem > 0.2) {
        dieKadr++;
        this->vrem = 0;
    }
}



Eye::Eye(Map* map, Hero* hero, bool isBoss) : Enemy(map, hero, isBoss) {
    // ��������� �������� �����
    if (isBoss) {
        enemy_scale = 1.4;
        health = 100 * enemy_scale * 3;
        enemy_speed = 90;
        enemy_height = 36 * enemy_scale;
        enemy_width = 27 * enemy_scale;
        cost = 30 * enemy_scale * 3;
        damage = 20 * 3;
    }
    else {
        enemy_scale = 1.0;
        health = 100;
        enemy_speed = 90;
        enemy_height = 33 * enemy_scale;
        enemy_width = 47 * enemy_scale;
        cost = 30;
        damage = 20;
    }


    // ��������
    enemy_Texture.loadFromFile("hero textures/Monsters/Flying eye/Flight.png");
    enemy_Run_Texture.loadFromFile("hero textures/Monsters/Flying eye/Flight.png");
    enemy_Attack_Texture.loadFromFile("hero textures/Monsters/Flying eye/Attack.png");
    enemy_dying_Texture.loadFromFile("hero textures/Monsters/Flying eye/Death.png");
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
    //


    // ������������� ��������� ������� ����� � ��������
    enemy_Sprite.setScale(enemy_scale, enemy_scale);
    enemy_Sprite.setOrigin(enemy_width / 2, enemy_height / 2);//������������� �����

}


void Eye::moveRightAnim() {
    if (kadr == 9) { kadr = 1; }
    if (kadr == 1) {
        enemy_Sprite.setTexture(enemy_Run_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
        kadr++;
    }
    else if (vrem > 0.07) {
        enemy_Sprite.setTextureRect(sf::IntRect(52 + (kadr - 1) * 102 + 48 * (kadr - 1), 48, 54, 54));
        vrem = 0;
        kadr++;
    }
}

void Eye::stayingAnim() {
    enemy_Sprite.setTexture(enemy_Texture);
    enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
    kadr = 1;
    vrem = 0;
}

void Eye::atackAnim() {

    if (kadrAttack >= 9) { kadrAttack = 1; }
    if (kadrAttack == 1) {
        enemy_Sprite.setTexture(enemy_Attack_Texture);
        enemy_Sprite.setTextureRect(sf::IntRect(52, 48, 54, 54));
        kadrAttack++;
    }
    else if (vrem > 0.06) {
        enemy_Sprite.setTextureRect(sf::IntRect(52 + (kadrAttack - 1) * 102 + 48 * (kadrAttack - 1), 48, 58, 54));
        vrem = 0;
        kadrAttack++;
    }
    if (kadrAttack == 8) {
        if (flagOfAttack == true) {
            hero->getDamage(damage);
            flagOfAttack = false;
        }
    }
    else {
        flagOfAttack = true;
    }


}

void Eye::dieAnim(double vrem) {
    enemy_Sprite.setTexture(enemy_dying_Texture);
    if (dieKadr == 1) { enemy_Sprite.setTextureRect(sf::IntRect(56, 63, 44, 22)); }

    if (dieKadr == 2) { enemy_Sprite.setTextureRect(sf::IntRect(209, 61, 36, 39)); }

    if (dieKadr == 3) { enemy_Sprite.setTextureRect(sf::IntRect(357, 77, 38, 25)); }

    if (dieKadr == 4) { enemy_Sprite.setTextureRect(sf::IntRect(507, 85, 52, 16)); }

    if (dieKadr == 5) { deleteEnemyList(this); }
    if (vrem > 0.2) {
        dieKadr++;
        this->vrem = 0;
    }
}






void Enemy::MakeEnemyList(Map* map, Hero* hero, bool isBoss) {
    EnemyList* thisList = new EnemyList;

    int x = rand() % 20;
    if (x >= 0 && x <=8) {

        Goblin* enemy = new Goblin(map, hero, isBoss);
        thisList->enemy = enemy;
    }

    if (x >= 9 && x <= 14) {

        Skeleton* enemy = new Skeleton(map, hero, isBoss);
        thisList->enemy = enemy;
    }
    if (x >= 15 && x <= 16) {

        Mushroom* enemy = new Mushroom(map, hero, isBoss);
        thisList->enemy = enemy;
    }
    if (x >= 17 && x <= 19) {
        Eye* enemy = new Eye(map, hero, isBoss);
        thisList->enemy = enemy;
    }



    if (n == 1) {
        thisEnemy->next = thisList;
        thisEnemy->prev = thisList;
        thisList->prev = thisEnemy;
        thisList->next = thisEnemy;
    }
    else if (n > 0) {
        thisList->next = thisEnemy->next;
        thisList->prev = thisEnemy;
        thisEnemy->next->prev = thisList;
        thisEnemy->next = thisList;
    }
    
    else {
        thisEnemy = thisList;
        thisList->prev = NULL;
        thisList->next = NULL;
    }
    n++;
    thisList->number = n;
}

void Enemy::deleteEnemyList(Enemy* enemy) {
    EnemyList* thisList;
    for (int i = n; i > 0; i--) {
        if (thisEnemy->enemy == enemy) {
            if (n > 1){
                thisEnemy->prev->next = thisEnemy->next;
                thisEnemy->next->prev = thisEnemy->prev;
                thisList = thisEnemy->next;
            }
            else {
                thisList = NULL;
            }
            Coin::MakeCoinList(thisEnemy->enemy, thisEnemy->enemy->hero);
            delete thisEnemy->enemy;
            delete thisEnemy;
            thisEnemy = thisList;
            n--;
            break;
        }
    }
}



Coin::CoinList* Coin::lastCoin = NULL;
Coin::CoinList* Coin::thisCoin = NULL;
int Coin::coins = 0;
Coin::Coin(Enemy* enemy, Hero* hero, bool isHeal) {
    coin_Position = enemy->getPosition();
    cost = enemy->cost;
    texture_size = 14;
    kadr = 1;
    vrem = 0;
    this->hero = hero;
    // ��������
    if (!isHeal) {
        coin_Texture.loadFromFile("hero textures/MonedaD.png");
        if (enemy->isBoss) {
            coin_Sprite.setColor(sf::Color(255, 0, 0));
        }
    }
    else {
        coin_Texture.loadFromFile("hero textures/heal.png");
    }
    coin_Sprite.setTexture(coin_Texture);
    coin_Sprite.setTextureRect(sf::IntRect(1, 0, 1 + texture_size, texture_size));

    
    coin_Sprite.setOrigin(texture_size / 2, texture_size / 2);//������������� �����
    coin_Sprite.setPosition(coin_Position);
}

sf::Sprite Coin::getSprite()
{
    return coin_Sprite;
}

void Coin::update(float elapsedTime)
{
    vrem += elapsedTime;
    if (!thisCoin->isHeal) {
        spinAnim();
    }
    if (abs(hero->getPosition().x - coin_Position.x)< 20 && abs(hero->getPosition().y - coin_Position.y) < 20) {
        if (!thisCoin->isHeal) { hero->payMoney(cost); }
        else { hero->heal(50); }
        deleteCoinList(this);
    }
}

void Coin::spinAnim() {
    
    if (kadr == 5) { kadr = 1; }
    if (kadr == 1) {
        coin_Sprite.setTextureRect(sf::IntRect(1, 0, texture_size, texture_size));
        kadr++;
    }
    else if (vrem > 0.2) {
        coin_Sprite.setTextureRect(sf::IntRect(1 + (kadr - 1) * 17 + texture_size * (kadr - 1), 0, texture_size, texture_size));
        vrem = 0;
        kadr++;
    }

}




void Coin::MakeCoinList(Enemy* enemy, Hero* hero) {
    CoinList* thisCoinList = new CoinList;
    int healOrCoin = rand() % 50;
    if (healOrCoin == 1) { thisCoinList->isHeal = true; }
    else { thisCoinList->isHeal = false; }
    Coin* coin = new Coin(enemy, hero, thisCoinList->isHeal);
    thisCoinList->coin = coin;


    if (coins == 1) {
        thisCoin->next = thisCoinList;
        thisCoin->prev = thisCoinList;
        thisCoinList->prev = thisCoin;
        thisCoinList->next = thisCoin;
    }
    else if (coins > 0) {
        thisCoinList->next = thisCoin->next;
        thisCoinList->prev = thisCoin;
        thisCoin->next->prev = thisCoinList;
        thisCoin->next = thisCoinList;
    }

    else {
        thisCoin = thisCoinList;
        thisCoinList->prev = NULL;
        thisCoinList->next = NULL;
    }
    coins++;
    thisCoinList->number = coins;
    
}

void Coin::deleteCoinList(Coin* coin) {
    CoinList* thisCoinList = NULL;
    for (int i = coins; i > 0; i--) {
        if (thisCoin->coin == coin) {
            if (coins > 1) {
                thisCoin->prev->next = thisCoin->next;
                thisCoin->next->prev = thisCoin->prev;
                thisCoinList = thisCoin->next;
            }
            delete thisCoin->coin;
            delete thisCoin;
            if (coins > 1){ thisCoin = thisCoinList; }
            
            coins--;
            break;
        }
    }
}