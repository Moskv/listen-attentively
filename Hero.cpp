#include "Hero.h"


Hero::Hero(Map* map)
{
    // ��������� �������� �����
    AllTime = 0;
    hero_height = 29;
    hero_width = 31;
    hero_vision = 7;
    dX = 0;
    dY = 0;

    ifstream file("money.txt");
    if (!file.is_open())
    {
        file.close();
        ofstream fout("money.txt");
        money = 0;
        fout << money;
        fout.close();
    }
    else
    {
        file >> money;
        file.close();
    }

    ifstream file2("speed.txt");
    if (!file2.is_open())
    {
        file2.close();
        ofstream fout2("speed.txt");
        hero_speed = 100;
        fout2 << hero_speed;
        fout2.close();
    }
    else
    {
        file2 >> hero_speed;
        file2.close();
    }
    ifstream file3("health.txt");
    if (!file3.is_open())
    {
        file3.close();
        ofstream fout3("health.txt");
        maxHealth = 50;
        fout3 << maxHealth;
        fout3.close();
    }
    else
    {
        file3 >> maxHealth;
        file3.close();
    }
    ifstream file4("damage.txt");
    if (!file4.is_open())
    {
        file4.close();
        ofstream fout4("damage.txt");
        damage = 20;
        fout4 << damage;
        fout4.close();
    }
    else
    {
        file4 >> damage;
        file4.close();
    }
    health = maxHealth;

    //
    // ��������
    image.loadFromFile("hero textures/main.png");
    image.createMaskFromColor(sf::Color::White);
    hero_Texture.loadFromImage(image);
    hero_Sprite.setTexture(hero_Texture);
    hero_Sprite.setTextureRect(sf::IntRect(8, 43, 31, 29));
    texture_size = 32;
    kadr = 1;
    vrem = 0;
    //
    //�����
    font.loadFromFile("hero textures/undertale battle font_0.ttf");
    moneyText = sf::Text("xuy", font, 15);
    timeText = sf::Text("xuy", font, 15);
    // 

    // ������������� ��������� ������� ����� � ��������
    hero_Position.x = (map->M * (texture_size +1)) / 2;
    hero_Position.y = (map->N * (texture_size+1)) / 2;
    
    //������
    view.reset(sf::FloatRect(0, 0, 640, 480));//������ ������
    circle = sf::CircleShape(texture_size * (hero_vision-1));//������������ ������
    circle.setFillColor(sf::Color(0, 0, 0, 0));
    circle.setPointCount(25);
    circle.setOutlineThickness(texture_size * (hero_vision+3));
    circle.setOutlineColor(sf::Color(0, 0, 0, 255));
    circle.setOrigin(texture_size * (hero_vision - 1), texture_size * (hero_vision - 1));
    //

    //������ �� ������ ��������
    this->map = map;
  
    //

    hero_Sprite.setOrigin(hero_width / 2, hero_height / 2);//������������� �����

    //�����
    buffersoundOfAttack.loadFromFile("hero textures/sword.ogg");
    soundOfAttack.setBuffer(buffersoundOfAttack);
    soundOfAttack.setVolume(20);

    bufferTakeHit.loadFromFile("hero textures/take_hit.ogg");
    takeHit.setBuffer(bufferTakeHit);
    takeHit.setVolume(20);

    bufferTakeHeal.loadFromFile("hero textures/healing.ogg");
    takeHeal.setBuffer(bufferTakeHeal);
    takeHeal.setVolume(20);

    buffersoundOfLost.loadFromFile("hero textures/Lost.ogg");
    soundOfLost.setBuffer(buffersoundOfLost);
    soundOfLost.setVolume(20);

    bufferTakeCoin.loadFromFile("hero textures/money_up.ogg");
    takeCoin.setBuffer(bufferTakeCoin);
    takeCoin.setVolume(30);

    lightBuf.loadFromFile("hero textures/light.wav");
    light.setBuffer(lightBuf);
    light.setVolume(15);
    light.setLoop(true);
    //������� � �����
    healthB.loadFromFile("hero textures/health_bar.png");
    healthBa.setTexture(healthB);
    healthBa.setTextureRect(sf::IntRect(0, 0, 40, 37));
    moneyB.loadFromFile("hero textures/money (1).png");
    moneyBa.setTexture(moneyB);
    moneyBa.setTextureRect(sf::IntRect(0, 0, 28, 37));

}

// ������ ��������� ������ ��������� ��� ������� draw()
sf::Sprite Hero::getSprite()
{
    return hero_Sprite;
}

sf::CircleShape Hero::getCircle() {
    return circle;
}

sf::Vector2f Hero::getPosition() {
    return hero_Position;
}

void Hero::takeMousCoor(sf::Vector2f pos) {
    this->pos = pos;
}

void Hero::rotateHero() {
    float dX = pos.x - hero_Position.x;//������ , ����������� ������, ������� ���������� ������ � ������
    float dY = pos.y - hero_Position.y;//�� ��, ���������� y
    float rotation = (atan2(dY, dX)) * 180 / 3.14159265;//�
    if (rotation < -90 || rotation > 90) {
        hero_Sprite.setScale(-1, 1);
    }
    else {
        hero_Sprite.setScale(1, 1);
    }
}

sf::Vector2i Hero::getHeroVision1() {
    sf::Vector2i vision;
    if (hero_Position.y / texture_size - hero_vision >= 0) {
        vision.y = int(hero_Position.y / texture_size - hero_vision);
    }
    else {
        vision.y = 0;
    }

    if (hero_Position.x / texture_size - hero_vision >= 0) {
        vision.x = int(hero_Position.x / texture_size - hero_vision);
    }
    else {
        vision.x = 0;
    }

    return vision;
}

sf::Vector2i Hero::getHeroVision2() {
    sf::Vector2i vision;
    if (hero_Position.y / texture_size + hero_vision < map->N) {
        vision.y = int(hero_Position.y / texture_size + hero_vision);
    }
    else {
        vision.y = map -> N - 1;
    }

    if ((hero_Position.x) / texture_size + hero_vision < map -> M) {
        vision.x = int(hero_Position.x / texture_size + hero_vision);
    }
    else {
        vision.x = map -> M - 1;
    }

    return vision;
}

sf::View Hero::getCamera() {
    return view;
}

void Hero::moveLeft()
{
    hero_LeftPressed = true;
}

void Hero::moveRight()
{
    hero_RightPressed = true;
}

void Hero::moveUp()
{
    hero_UpPressed = true;
}

void Hero::moveDown()
{
    hero_DownPressed = true;
}

void Hero::atack()
{
    hero_atack = true;
}

void Hero::lighter()
{
    hero_lighter = true;
    light.play();
}

void Hero::stopLeft()
{
    hero_LeftPressed = false;
}

void Hero::stopRight()
{
    hero_RightPressed = false;
}

void Hero::stopUp()
{
    hero_UpPressed = false;
}

void Hero::stopDown()
{
    hero_DownPressed = false;
}

void Hero::stopAtack()
{
    hero_atack = false;
    kadrAttack = 1;
}

void Hero::stopLighter()
{
    hero_lighter = false;
    kadrAttack = 1;
    light.stop();
}


// ������� ���� �� ��������� ����������������� ����� � ���� �����,
// ���������� ������� � ��������
void Hero::update(float elapsedTime)
{
    vrem += elapsedTime;
    AllTime += elapsedTime;
    if (dying) {
        dyingAnim(vrem);
        return;
    }
    getAtack();
    if (hero_atack) {
        atackAnim();
    }
    else if (hero_lighter) {
        lighterAnim();
    }
    else {
        trapAttacked(elapsedTime);
        if (hero_RightPressed)
        {
            hero_Position.x += hero_speed * elapsedTime;
            heroLimitation('r');
            moveRightAnim();

        }

        if (hero_LeftPressed)
        {
            hero_Position.x -= hero_speed * elapsedTime;
            heroLimitation('l');
            moveRightAnim();
        }

        if (hero_UpPressed)
        {
            hero_Position.y -= hero_speed * elapsedTime;
            heroLimitation('w');
            moveRightAnim();



        }

        if (hero_DownPressed)
        {
            hero_Position.y += hero_speed * elapsedTime;
            heroLimitation('s');
            moveRightAnim();


        }

        if (!hero_RightPressed && !hero_LeftPressed && !hero_UpPressed && !hero_DownPressed) {
            stayingAnim();
        }

    }
    
    // ������ �������� ������ �� ����� �������
    hero_Sprite.setPosition(hero_Position);
    circle.setPosition(hero_Position.x, hero_Position.y);
    centralCamera();

    //������ � �����
    std::ostringstream playerMoney;
    std::ostringstream allTime;
    allTime << int(AllTime) / 60 << ":" << int(AllTime)%60;
    playerMoney << money;
    timeText.setString(allTime.str());
    timeText.setPosition(hero_Position.x - 15, hero_Position.y - 205);
    moneyText.setString(playerMoney.str());
    moneyText.setPosition(hero_Position.x + 260, hero_Position.y - 205);//������ ������� ������
    moneyBa.setPosition(hero_Position.x + 300, hero_Position.y - 220);//������ ������� ������
    healthBa.setTextureRect(sf::IntRect(0, 0, 40, int(37 * ((float(health) / (float(maxHealth)/100)) * 0.01))));
    healthBa.setPosition(hero_Position.x - 290, hero_Position.y - 220);
    //

}

void Hero::stayingAnim() {
    if (kadr >= 5) { kadr = 1; }
    if (kadr == 1) {
        hero_Sprite.setTexture(hero_Texture);
        hero_Sprite.setTextureRect(sf::IntRect(7, 4, 31, 31));
        kadr++;
    }
    else if (vrem > 0.3) {
        hero_Sprite.setTextureRect(sf::IntRect(7 + (kadr - 1) * 18 + 31 * (kadr - 1), 4, 31, 31));
        vrem = 0;
        kadr++;
    }
}

void Hero::moveRightAnim() {
    if (kadr >= 7) { kadr = 1; }
    if (kadr == 1) {
        hero_Sprite.setTexture(hero_Texture);
        hero_Sprite.setTextureRect(sf::IntRect(8, 43, 31, 29));
        kadr++;
    }
    else if (vrem > 0.07) {
        hero_Sprite.setTextureRect(sf::IntRect(8+(kadr-1)*18+31*(kadr-1), 43, 31, 29));
        vrem = 0;
        kadr++;
    }
}

void Hero::atackAnim() {
    if (kadrAttack >= 8) { kadrAttack = 1; }
    if (kadrAttack == 1) {
        hero_Sprite.setTexture(hero_Texture);
        hero_Sprite.setTextureRect(sf::IntRect(8, 111, 35, 35));
        kadrAttack++;
    }
    
    else if (vrem > 0.06) {
        hero_Sprite.setTextureRect(sf::IntRect(8 + (kadrAttack - 1) * 18 + 31 * (kadrAttack - 1), 111, 42, 35));
        vrem = 0;
        kadrAttack++;
    }

    if (kadrAttack == 5) {

        hero_atack2 = true;
        soundOfAttack.play();
 
    }
    else {
        hero_atack2 = false;
    }
}

void Hero::lighterAnim() {
    if (kadrAttack >= 7) { kadrAttack -= 1; }
    if (kadrAttack == 1) {
        hero_Sprite.setTexture(hero_Texture);
        hero_Sprite.setTextureRect(sf::IntRect(8, 153, 35, 35));
        kadrAttack++;
    }

    else if (vrem > 0.15) {
        hero_Sprite.setTextureRect(sf::IntRect(8 + (kadrAttack - 1) * 18 + 31 * (kadrAttack - 1), 153, 42, 35));
        vrem = 0;
        kadrAttack++;
        
    }

    if (kadrAttack == 7) {

        hero_lighter2 = true;
        

    }
    else {
        hero_lighter2 = false;
        
    }
}

void Hero::dyingAnim(float vrem) {
    hero_Texture.loadFromFile("hero textures/dying.png");
    hero_Sprite.setTexture(hero_Texture);
    if (dieKadr == 1) { hero_Sprite.setTextureRect(sf::IntRect(15, 339, 17, 29)); }

    if (dieKadr == 2) { hero_Sprite.setTextureRect(sf::IntRect(63, 339, 17, 29)); }

    if (dieKadr == 3) { hero_Sprite.setTextureRect(sf::IntRect(111, 339, 17, 29)); }

    if (dieKadr == 4) { hero_Sprite.setTextureRect(sf::IntRect(159, 339, 17, 29)); }

    if (dieKadr == 5) { hero_Sprite.setTextureRect(sf::IntRect(206, 354, 36, 14)); }
    if (dieKadr == 6) { 
        soundOfLost.play();
        needToRestart = true; }
    if (vrem > 0.4) {
        dieKadr++;
        this->vrem = 0;
    }
}

void Hero::getAtack() {
    if (hero_attacked || hero_attackedFromTrap) {
        hero_Sprite.setColor(sf::Color::Red);
    }
    else {
        hero_Sprite.setColor(sf::Color::White);
    }
}


void Hero::trapAttacked(float vrem) {
    if (hero_attackedFromTrap) {
        timeAttacked += vrem;
        if (timeAttacked > 1.4) {
            hero_attackedFromTrap = false;
        }
        else if (timeAttacked > 0.3) {
            hero_Sprite.setColor(sf::Color::White);
        }
    }
    else {

        if (map->grid[int(hero_Position.y / texture_size)][int(hero_Position.x / texture_size)] == '@')
        {
            if (!hero_attackedFromTrap) {
                if (map->kadr >= 9 && map->kadr <= 12) {
                    hero_attackedFromTrap = true;
                    getDamage(20);
                    timeAttacked = 0;
                }
            }

        }
    }
}

void Hero::heroLimitation(char direction) {

    
    if (direction == 'w') {
        for (int i = hero_Position.x - hero_width / 2; i < (hero_Position.x + hero_width / 2); i += hero_width / 4) {
            if (map->grid[int((hero_Position.y - hero_height / 2) / texture_size)][int(i / texture_size)] == '0') {
                hero_Position.y = int(hero_Position.y / texture_size) * texture_size + hero_height / 2;
                break;
            }
        }
            
        
    }
    if (direction == 's') {
        for (int i = hero_Position.x - hero_width / 2; i < (hero_Position.x + hero_width / 2); i += hero_width / 4) {
            if (map->grid[int((hero_Position.y + hero_height / 2) / texture_size)][int(i / texture_size)] == '0') {
                hero_Position.y = int(hero_Position.y / texture_size) * texture_size + texture_size - hero_height / 2;

            }
        }

    }
    if (direction == 'l') {
        for (int i = hero_Position.y - hero_height / 2; i < (hero_Position.y + hero_height / 2); i += hero_height / 4) {
            if (map->grid[int(i / texture_size)][int((hero_Position.x - hero_width / 2) / texture_size)] == '0') {
                hero_Position.x = int((hero_Position.x) / texture_size) * texture_size + hero_width / 2;

            }
        }

    }
    if (direction == 'r') {
        for (int i = hero_Position.y - hero_height / 2; i < (hero_Position.y + hero_height / 2); i += hero_height / 4) {
            if (map->grid[int(i / texture_size)][int((hero_Position.x + hero_width / 2) / texture_size)] == '0') {
                hero_Position.x = int(hero_Position.x / texture_size) * texture_size + texture_size - hero_width / 2;

            }
        }

        
    }
}

void Hero::centralCamera() {
    view.setCenter(hero_Position.x + 15, hero_Position.y + 15);
}

void Hero::payMoney(int money) {
    this->money += money;
    takeCoin.play();
}

void Hero::heal(int heal) {
    if (maxHealth - health > heal) {
        health += heal;
        
    }
    else {
        health = maxHealth;
    }
    takeHeal.play();
}

int Hero::getMoney() {
    return money;
}

void Hero::getDamage(int damage) {
    health -= damage;
    takeHit.play();
    if (health <= 0) {
        dying = true;
    }
}

int Hero::returnDamage() {
    return damage;
}
