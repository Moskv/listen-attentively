#include "Engine.h"

void Engine::MakeMenu(sf::RenderWindow* window) {
	
	Menu menu(hero->getPosition().x, hero->getPosition().y);
	
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					//isMenu = false;
					//return ;
				case sf::Keyboard::Up:
					menu.MoveUp();
					break;

				case sf::Keyboard::Down:
					menu.MoveDown();
					break;

				case sf::Keyboard::Return:
					switch (menu.GetPressedItem())
					{
					case 0:
						if (hero->needToRestart) {
							needToRestart = true;
							quiteGame();
						}
						isMenu = false;
						return ;
					case 1:
						MakePw(window);
						break;
					case 2:
						quiteGame();
					}

					break;
				}

				break;
			case sf::Event::Closed:
				window->close();

				break;

			}

		}
		window->clear();
		menu.draw(m_Window);
		window->display();
	}
}

void Engine::MakePw(sf::RenderWindow* window) {

	PowerUp pw(hero->getPosition().x, hero->getPosition().y, hero);

	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					//isMenu = false;
					//return ;
				case sf::Keyboard::Up:
					pw.MoveUp();
					break;

				case sf::Keyboard::Down:
					pw.MoveDown();
					break;

				case sf::Keyboard::Return:
					switch (pw.GetPressedItem())
					{
					case 0:
						pw.speedUp();
						saveGame();
						return ;
					case 1:
						pw.healthUp();
						saveGame();
						return;
					case 2:
						pw.damageUp();
						saveGame();
						return;
					case 3:
						return ;
					}

					break;
				}

				break;
			case sf::Event::Closed:
				window->close();

				break;

			}

		}
		window->clear();
		pw.draw(m_Window);
		window->display();
	}
}

Engine::Engine()
{
	
    // �������� ���������� ������, ������� ���� SFML � View
    
    resolution.x = VideoMode::getDesktopMode().width;
    resolution.y = VideoMode::getDesktopMode().height;

    m_Window = new RenderWindow();

    m_Window->create(VideoMode(resolution.x, resolution.y),
        "Simple Game Engine",
        Style::Fullscreen);


	isMenu = false;
    //��������� �����
    map = new Map();
    // ��������� ��������
    hero = new Hero(map);
    //��������� �����
	//������� ������ ������
	music = new sf::Music();
	music->openFromFile("hero textures/fon_music.ogg");
	music->setLoop(true);
	music->setVolume(5);

}

Engine::~Engine()
{
	delete m_Window;
	delete hero;
	delete map;
	delete music;
}


Vector2f Engine::mousePos() {
    return pos;
}


void Engine::start()
{
    // ������ �������
	int minitime = 0;
	int lastminitime = 0;
    Clock clock;
	srand(time(0));
	music->play();
    while (m_Window->isOpen())
    {
		if (isMenu) { MakeMenu(m_Window); 
		clock.restart();
		}
        //���������� �������
        pixelPos = Mouse::getPosition(*m_Window);//�������� ����� �������
        pos = (*m_Window).mapPixelToCoords(pixelPos);//��������� �� � ������� (������ �� ����� ����)
        // ������������� ������ � ���������� ���������� ����� � dt
        

		Time dt = clock.restart();
		float dtAsSeconds = dt.asSeconds();
        input();
        update(dtAsSeconds);
        draw();
		
		minitime = int(hero->AllTime);

		

		if (minitime - lastminitime == 10) {
			lastminitime = minitime;
			if (minitime % 120 == 0) {
				Enemy::MakeEnemyList(map, hero, true);
			}
			else {
				for (int i = 0; i < int(hero->AllTime) / 10; i++) {
					Enemy::MakeEnemyList(map, hero);
				}
			}
		}
		restart(hero->needToRestart);
    }
}

void Engine::saveGame() {
	ofstream file("money.txt");
	file << hero->getMoney();
	file.close();
	ofstream file2("speed.txt");
	file2 << hero->hero_speed;
	file2.close();
	ofstream file3("health.txt");
	file3 << hero->maxHealth;
	file3.close();
	ofstream file4("damage.txt");
	file4 << hero->damage;
	file4.close();
}
void Engine::quiteGame() {
	saveGame();
	m_Window->close();
}

void Engine::restart(bool rest) {
	if (rest) {
		for (int i = Enemy::n; i > 0; i--) {	
			Enemy::deleteEnemyList(Enemy::thisEnemy->enemy);
		}

		for (int i = Coin::coins; i > 0; i--) {
			Coin::deleteCoinList(Coin::thisCoin->coin);
		}
		saveGame();
		MakeMenu(m_Window);
	}
}