#pragma once

#include <SFML/Graphics.hpp>
#include "Hero.h"
#include <SFML/Audio.hpp>

class Enemy {
protected:
    // �������� �����
    sf::Vector2f enemy_Position;
    float enemy_speed;
    float enemy_height;
    float enemy_width;
    float enemy_scale;
    int enemy_vision;
    int health;
    int damage;

    sf::Vector2f pos;
    //
    // 
    // ���������� ���������� ��� ������������ ����������� �������� � ������
    bool enemy_LeftPressed;
    bool enemy_RightPressed;
    bool enemy_UpPressed;
    bool enemy_DownPressed;
    bool seeing_Left;
    bool dying = false;

    //

    // ��������
    sf::Sprite enemy_Sprite;
    sf::Texture enemy_Texture;


    //�������� ����
    sf::Texture enemy_Run_Texture;
    sf::Texture enemy_Attack_Texture;
    sf::Texture enemy_dying_Texture;
    float vrem;//������� �������
    int kadr;//���� ��������
    int kadrAttack;
    int heroAttackKadr = 1;
    bool flagOfAttack = true; // ������� :)
    int dieKadr = 1;
    int texture_size;
    //
    bool enemy_attackedFromTrap = false;
    float timeAttacked = 0;



    //���������

    Map* map;
    Hero* hero;

    
    struct EnemyList {
        Enemy* enemy;
        EnemyList* prev;
        EnemyList* next;
        int number;
    };
    

    sf::SoundBuffer bufferTakeHit;
    sf::Sound takeHit;
    
    
public:
    bool isBoss;
    int cost;
    bool hero_attacked = false;
    static int n;
    static EnemyList* lastEnemy;
    static EnemyList* thisEnemy;
    // �����������
    Enemy(Map*, Hero*, bool);

    // ��� �������� 
    sf::Sprite getSprite();
    sf::Vector2f getPosition();


    // ��� �������� �����
    void moveLeft();
    void moveUp();
    void moveDown();
    void moveRight();
    void virtual moveRightAnim() = 0;



    //
    // ����������� ��������
    void stopLeft();
    void stopUp();
    void stopDown();
    void stopRight();
    void virtual stayingAnim() = 0;
    void virtual atackAnim() = 0;
    void virtual dieAnim(double) = 0;

    //������ �� ������
    void checkHero(sf::Vector2f hero_Position);
    bool checkAtack();
    static void MakeEnemyList(Map* map, Hero* hero, bool isBoss = false);
    static void deleteEnemyList(Enemy*);

    //������������ � ������
    void enemyLimitation();
    void trapAttacked(float);

    // ��� ������� ����� ���������� �� ������ ����
    void update(float elapsedTime);
    
};

class Goblin : public Enemy {
public:
    void moveRightAnim();
    void stayingAnim();
    void atackAnim();
    void dieAnim(double);
    Goblin(Map*, Hero*, bool);
};

class Skeleton : public Enemy {
public:
    void moveRightAnim();
    void stayingAnim();
    void atackAnim();
    void dieAnim(double);
    Skeleton(Map*, Hero*, bool);
};

class Mushroom : public Enemy {
public:
    void moveRightAnim();
    void stayingAnim();
    void atackAnim();
    void dieAnim(double);
    Mushroom(Map*, Hero*, bool);
};

class Eye : public Enemy {
public:
    void moveRightAnim();
    void stayingAnim();
    void atackAnim();
    void dieAnim(double);
    Eye(Map*, Hero*, bool);
};

class Coin {
private:
    int cost;
    sf::Vector2f coin_Position;
    // ��������
    sf::Sprite coin_Sprite;
    sf::Texture coin_Texture;
    float vrem;//������� �������
    int kadr;//���� ��������
    int texture_size;

    struct CoinList {
        Coin* coin;
        CoinList* prev;
        CoinList* next;
        int number;
        bool isHeal;
    };

    Hero* hero;
    
    void spinAnim();
    //�����

public:
    sf::Sprite getSprite();
    static void MakeCoinList(Enemy*, Hero*);
    static void deleteCoinList(Coin*);
    static int coins;
    static CoinList* lastCoin;
    static CoinList* thisCoin;
    Coin(Enemy*, Hero*, bool);
    void update(float elapsedTime);
};