#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Map.h"





class Hero{
private:

    // �������� �����
    sf::Vector2f hero_Position;

    int hero_height;
    int hero_width;
    int hero_vision;
    //��� ������������ �������
    float dX;
    float dY;
    sf::Vector2f pos;
    //
    // 
    // ���������� ���������� ��� ������������ ����������� ��������
    bool hero_LeftPressed;
    bool hero_RightPressed;
    bool hero_UpPressed;
    bool hero_DownPressed;
    bool hero_atack;
    bool hero_lighter;
    bool hero_attackedFromTrap = false;
    bool dying = false;
    float timeAttacked = 0;
    //

    // ��������
    sf::Image image;
    sf::Sprite hero_Sprite;
    sf::Texture hero_Texture;
    

    sf::Texture healthB;
    sf::Texture moneyB;
    //�������� ����
    sf::Texture hero_Run_Texture;
    float vrem;//������� �������
    

    int texture_size;
    //
    
    //������
    sf::View view;
    void centralCamera();
    sf::CircleShape circle;//����������� ������

    //�����
    sf::Font font;
    //
  
    //���������
    Map* map;

    //�����
    sf::SoundBuffer buffersoundOfAttack;
    sf::Sound soundOfAttack;
    sf::SoundBuffer bufferTakeHit;
    sf::Sound takeHit;
    sf::SoundBuffer buffersoundOfLost;
    sf::Sound soundOfLost;
    sf::SoundBuffer bufferTakeHeal;
    sf::Sound takeHeal;
    sf::SoundBuffer bufferTakeCoin;
    sf::Sound takeCoin;
    sf::SoundBuffer lightBuf;
    sf::Sound light;
    //

public:
    float AllTime;
    sf::Sprite healthBa;
    sf::Sprite moneyBa;

    int damage;
    int money;
    int health;
    int maxHealth;
    float hero_speed;
    int kadr;//���� ��������
    bool hero_atack2;
    bool hero_lighter2;
    int kadrAttack;//����� �����
    bool hero_attacked = false; // �������� �� �����
    // �����������
    Hero(Map*);

    // ��� �������� 
    sf::Sprite getSprite();
    sf::View getCamera();
    sf::Vector2i getHeroVision1();
    sf::Vector2i getHeroVision2();
    sf::Vector2f getPosition();
    sf::CircleShape getCircle();

    //��� ��������
    void takeMousCoor(sf::Vector2f);
    //������ �� �����
    void rotateHero();

    // ��� �������� �����
    void moveLeft();
    void moveUp();
    void moveDown();
    void moveRight();
    void atack();
    void stopAtack();
    void lighter();
    void stopLighter();
    void moveRightAnim();
    void dyingAnim(float);
    int dieKadr = 1;
    void atackAnim();
    void lighterAnim();
    void getAtack();
    // ����������� ��������
    void stopLeft();
    void stopUp();
    void stopDown();
    void stopRight();
    void stayingAnim();

    //������������ � ������
    void heroLimitation(char);
    void trapAttacked(float);

    // ��� ������� ����� ���������� �� ������ ����
    void update(float elapsedTime);
    
    void payMoney(int);
    void heal(int);
    int getMoney();
    void getDamage(int);
    int returnDamage();
    
    //������
    sf::Text moneyText;
    sf::Text timeText;

    bool needToRestart = false;
};