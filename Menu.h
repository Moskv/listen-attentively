#pragma once
#include "SFML/Graphics.hpp"
#include "Hero.h"
#include <string>
#define MAX_NUMBER_OF_ITEMS 3

class Menu
{
public:
	Menu(float width, float height);
	~Menu();

	void draw(sf::RenderWindow* window);
	void MoveUp();
	void MoveDown();
	int GetPressedItem() { return selectedItemIndex; }
private:
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
};


#define MAX_NUMBER_OF_ITEMS2 4

class PowerUp
{
public:
	PowerUp(float width, float height, Hero*);
	~PowerUp();

	void draw(sf::RenderWindow* window);
	void MoveUp();
	void MoveDown();
	int GetPressedItem() { return selectedItemIndex; }
	void speedUp();
	void healthUp();
	void damageUp();
private:
	Hero* hero;
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS2];
	int speed;
	int health;
	int money;
	int damage;
};