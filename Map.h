#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>
#include <ctime>

using namespace std;

class Map {
public:
	sf::Image map_image;//������ ����������� ��� �����
	sf::Image trap_image;//������ ����������� ��� �����
	sf::Texture map;//�������� �����
	sf::Sprite s_map;//������ ������ ��� �����	
	sf::Texture traps;//�������� �����
	sf::Sprite s_trap;//������ ������ ��� �����	

	static const int N = 500; // ������� ����� �� Y (� ���� x)
	static const int M = 1000; // ������� ����� �� � (� ���� y)

	char grid[N][M];

	int numberOfTraps;
	char border = '0'; // �������
	char prefab = ' '; // ���
	char trap = '@'; // �������

	bool around(int X, int Y, int W, int H);
	void GenerateBorders();
	void TrapGenerate();

	Map();
	sf::Sprite drawMap(int, int);
	Map* returnMap();
	void update(float);
	float vrem;
	int kadr = 0;
	bool needToDrawTrap = false;
};



