#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Enemys.h"
#include "Menu.h"
#include <math.h>
using namespace sf;

class Engine
{
private:
    float AllTime;
    int minute;
    Vector2f resolution;
    bool isMenu = false;
    bool isPowerUp = false;
    RenderWindow* m_Window;
    //�������� ���������� ����
    Vector2i pixelPos; 
    Vector2f pos;
    // ��������� ������ � �������� ��� ����
    Sprite m_BackgroundSprite;
    Texture m_BackgroundTexture;

    void MakeMenu(sf::RenderWindow*);
    void MakePw(sf::RenderWindow*);
    void input();
    void update(float dtAsSeconds);
    void draw();
    void restart(bool);
    Vector2f mousePos();

	//��������� �����
	Map* map;
    // ��������� �����
    Hero* hero;

    void saveGame();
    void quiteGame();
    sf::Music* music;
public:
    
    // ����������� ������
    Engine();
    ~Engine();
    // ������� ����� ������� ��� ��������� �������
    void start();
    bool needToRestart = false;
    
};
