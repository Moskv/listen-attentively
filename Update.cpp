#include "Engine.h"

using namespace sf;

void Engine::update(float dtAsSeconds)
{
    
   bool flag = false;
   for (int i = Enemy::n; i > 0; i--) {
       if (Enemy::thisEnemy->enemy->hero_attacked) { flag = true; }
       Enemy::thisEnemy->enemy->update(dtAsSeconds);
       if (Enemy::n <= 1) { continue; }
       Enemy::thisEnemy = Enemy::thisEnemy->next; 
       
   }
   if (flag) { hero->hero_attacked = true;}
   else{ hero->hero_attacked = false; }

   for (int i = Coin::coins; i > 0; i--) {
       Coin::thisCoin->coin->update(dtAsSeconds);
       if (Coin::coins <= 1) { continue; }
       Coin::thisCoin = Coin::thisCoin->next;
   }
   map->update(dtAsSeconds);
   hero->update(dtAsSeconds);
}